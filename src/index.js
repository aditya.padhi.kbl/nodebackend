const express = require('express');
const app = express();
const port = process.env.PORT || 3000;

app.get("/", (request, response) => {
    response.send("Hello from Express!!!")
});

app.listen(port, (err) => {
    if (err) {
        console.log("something went wrong ", err);
    }

    console.log(`server is listening on ${port}`);
});